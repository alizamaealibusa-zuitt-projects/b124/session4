package com.alibusa;

public class Human {
    // Mini-activity:
    // Create a new Human class with the following properties and methods:
    // Properties:
    // name, age, gender (single character)
    // Methods:
    // talk --> display a message to the console "Hello, my name is " concatenated to the user's name
    // setters and getters methods for the properties
    // Constructor: implement both empty and parameterized

    private String name;
    private int age;
    private char gender;

    // Empty Constructors
    public Human() {}

    // Getters
    public String getName() {
        return this.name;
    }
    public int getAge() {
        return this.age;
    }
    public char getGender() {
        return this.gender;
    }

    // Setters
    public void setName(String newName) {
        this.name = newName;
    }
    public void setAge(int newAge) {
        this.age = newAge;
    }
    public void setGender(char newGender) {
        this.gender = newGender;
    }

    // Parameterized Constructors
    public Human(String newName, int newAge, char newGender) {
        this.name = newName;
        this.age = newAge;
        this.gender = newGender;
    }

    // Method : Display Talk
    public String talk() {
        return "Hello, my name is " + this.getName() + "!";
    }
}
