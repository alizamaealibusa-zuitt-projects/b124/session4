package com.alibusa;

public class Main {

    public static void main(String[] args) {

        //Empty Constructors
            Human a = new Human();
            a.setName("John Swift");
            a.setAge(20);
            a.setGender('M');
            System.out.println("First Name: " + a.getName());
            System.out.println("Age: " + a.getAge());
            System.out.println("Gender: " + a.getGender());

        // Parameterized Constructors (Human)
            Human b = new Human("Jane Smith", 24, 'F');
            System.out.println("First Name: " + b.getName());
            System.out.println("Age: " + b.getAge());
            System.out.println("Gender: " + b.getGender());

        // Method : Display Talk
            Human c = new Human("Kevin Ross", 21, 'M');
            System.out.println(c.talk());

        // Parameterized Constructors (Adult)
            Adult adult1 = new Adult("Juan Cruz", 29, 'M',"Banker");
            System.out.println(adult1.talk());
            // System.out.println("First Name: " + adult1.getName());
            // System.out.println("Age: " + adult1.getAge());
            // System.out.println("Gender: " + adult1.getGender());
            System.out.println("Occupation: " + adult1.getOccupation());

        // Parameterized Constructors (Child)
            Child child1 = new Child("Bea Garcia", 19, 'F',"Computer Science Major");
            System.out.println(child1.talk());
            // System.out.println("First Name: " + child1.getName());
            // System.out.println("Age: " + child1.getAge());
            // System.out.println("Gender: " + child1.getGender());
            System.out.println("Education: " + child1.getEducation());

    }
}
