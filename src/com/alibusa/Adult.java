package com.alibusa;

public class Adult extends Human {
    // Create a property occupation
        private String occupation;

    // Create an empty constructor
        public Adult() {}

    // Create a parameterized constructor
        public Adult(String name, int age, char gender, String newOccupation) {
            super(name, age, gender);
            this.occupation = newOccupation;
        }

    // Getters
    public String getOccupation() {
        return this.occupation;
    }

    // Setters
    public void setOccupation(String newOccupation) {
        this.occupation = getOccupation();
    }

    // Customized talk method --> polymorphism
    public String talk() {
        return "Hello, I'm " + this.getName() + "! And I'm an adult.";
    }
}
