package com.alibusa;

public class Child extends Human {
    // Create a property education
        private String education;

    // Create an empty constructor
        public Child() {}

    // Create a parameterized constructor
        public Child(String name, int age, char gender, String newEducation) {
            super(name, age, gender);
            this.education = newEducation;
        }

    // Getters
    public String getEducation() {
        return this.education;
    }

    // Setters
    public void setEducation(String newEducation) {
        this.education = getEducation();
    }

}
